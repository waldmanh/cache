# Cache
A class that represents a cache.
The vision of the project is to *cache* the latest data (based on the project needs).
As a result of the above, the expected result is a major improvement in the query response time.
Note that this implementation is not saving the cache in any "hard" measurements.
This means that only while the Cache instance is alive - it's useable.

Enjoy!
