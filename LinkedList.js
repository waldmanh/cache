class LinkedListNode{
    constructor(prev, value, next ){
        this.prev = prev
        this.value = value
        this.next = next
    }    
}

module.exports = LinkedListNode