const LinkedListNode = require("./LinkedList")

// Cache implemented with O(1)
class CacheMI {

    constructor(maxItems) {
        console.log('Cache created!')
        this.cacheList = {}
        this.head = null
        this.tail = null
        this.maxItems = maxItems
    }

    set(key, value) {
        if (typeof (key) !== "string") {
            console.log(`Key type is ${typeof (key)} instead of string. `)
            return
        }

        if (Object.keys(this.cacheList).length === 0) {
            this.cacheList[key] = new LinkedListNode(null, value, null)
            this.head = key
            this.tail = key
            return
        }
        
        this.setTailIfNew(key)
        if (this.cacheList[key] === undefined) {
            this.createNewNode(key, value)
        } else {
            this.removeNodeLinks(key)
            this.setTailIfLast(key)
        }
        this.setAsHead(key)
    }

    get(key) {
        console.log('get = ', key)
        if (this.cacheList[key] === undefined) {
            return undefined
        }
        this.removeNodeLinks(key)
        if (key != this.head) {
            this.setAsHead(key)
        }
        return this.cacheList[key].value
    }

    toObject() {
        console.log('toObject')
        return this.cacheList
    }
    setAsHead(key) {
        if (this.head != key) {
            this.cacheList[this.head].prev = key
            this.cacheList[key].prev = null
            this.cacheList[key].next = this.head
        }
        this.head = key
    }
    createNewNode(key, value) {
        this.cacheList[key] = new LinkedListNode(null, value, this.head)
        this.cacheList[this.head].prev = key
    }
    removeNodeLinks(key) {
        let tmpPrev = this.cacheList[key].prev
        let tmpNext = this.cacheList[key].next

        if (tmpPrev != null && this.head != key) {
            this.cacheList[tmpPrev].next = tmpNext
        }
        if (tmpNext != null && this.head != key) {
            this.cacheList[tmpNext].prev = tmpPrev
        }        
    }

    setTailIfNew(key) {
        if (this.cacheList[key] === undefined && Object.keys(this.cacheList).length === this.maxItems) {
            let prevTail = this.cacheList[this.tail].prev
            delete (this.cacheList[this.tail])
            this.cacheList[prevTail].next = null
            this.tail = prevTail
        }
    }
    setTailIfLast(key){
        if(this.tail === key){
            this.tail = this.cacheList[key].prev
        }
    }
}

module.exports = CacheMI